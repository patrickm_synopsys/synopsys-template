# Synopsys Template

The latest version of the Synopsys Bridge is available at: [Synopsys Bridge](https://sig-repo.synopsys.com/artifactory/bds-integrations-release/com/synopsys/integration/synopsys-action/)

Synopsys Template allows you to configure your pipeline to run Synopsys security testing and take action on the security results. Synopsys Template leverages the Synopsys Bridge, a foundational piece of technology that has built-in knowledge of how to run all major Synopsys security testing solutions.

**Please Note:** Synopsys Template requires the appropriate licenses for the Synopsys security testing solutions (E.g. Polaris, Coverity, or Black Duck).

# Quick Start for the Synopsys Template

Synopsys Template supports all major Synopsys security testing solutions:
- Polaris, our SaaS-based solution that offers SAST, SCA and Managed Services in a single unified platform
- Coverity, using our thin client and cloud-based deployment model
- Black Duck Hub, supporting either on-premises or hosted instances

In this Quick Start, we will provide individual examples for each Synopsys security testing solution, but in practice, the options can be combined to run multiple solutions from a single .gitlab-ci.yml file.

This process will:
- Validate Scanning platform-related parameters like project and stream
- Download the Synopsys Bridge and related adapters
- Run corresponding Synopsys security testing solutions (E.g. Polaris, Coverity, or Black Duck). 
- Synopsys solution functionality is invoked directly by the Synopsys Bridge, and indirectly by the Synopsys Template which will download the bridge and call the respective adapters to run corresponding scan.

## Synopsys Template - Polaris

Before you can run a pipeline using the Synopsys Template and Polaris, you have to add .gitlab-ci.yml to your project with given line of codes in code section.

We recommend configuring sensitive data like access tokens and even URLs, using gitlab secrets.

Once changes to your branch is pushed, gitlab runner will pickup job and initiates the pipeline.

```yaml
include:
    - project: synopsys/synopsys-template
      ref: main
      file: templates/unified-template.yml
  stages:
    - gitlab-template-executor
  check_template_job:
    stage: gitlab-template-executor
    tags:
      - macos
    variables:
      POLARIS_SERVER_URL: ${{ secrets.POLARIS_SERVER_URL }}
      POLARIS_ACCESS_TOKEN: ${{ secrets.POLARIS_ACCESS_TOKEN }}
      POLARIS_APPLICATION_NAME: 'testapp1'
      POLARIS_PROJECT_NAME: 'testproj1'
      # Accepts Multiple Values.
      POLARIS_ASSESSMENT_TYPES: 'SCA,SAST'
    extends:
      - .run-synopsys-tools
```
# Synopsys Template - Coverity Cloud Deployment with Thin Client

Please note that Synopsys Template at this time supports only the Coverity cloud deployment model (Kubernetes-based)
which uses a small footprint thin client to capture the source code, and then submit an analysis job that runs on the server.
This removes the need for a large footprint (many GB) software installation in your gitlab Runner.

Before you can run a pipeline using the Synopsys Template and Coverity, you must make sure the appropriate
project and stream are set in your Coverity Connect server environment.

We recommend configuring sensitive data like username and password, and even URL, using gitlab secrets.

```yaml

include:
    - project: synopsys/synopsys-template
      ref: main
      file: templates/unified-template.yml
  stages:
    - gitlab-template-executor
  check_template_job:
    stage: gitlab-template-executor
    tags:
      - macos
    variables:
      COVERITY_URL: ${{ secrets.COVERITY_URL }}
      COVERITY_USER: ${{ secrets.COVERITY_USER }}
      COVERITY_PASSWORD: ${{ secrets.COVERITY_PASSWORD }}
      COVERITY_PROJECT_NAME: 'coverity-project-name'
      COVERITY_STREAM_NAME: 'coverity-stream-name'
    extends:
      - .run-synopsys-tools
```
## Synopsys Template - Black Duck 

Synopsys Template supports both self-hosted (e.g. on-prem) and Synopsys-hosted Black Duck Hub instances.

In the default Black Duck Hub permission model,
projects and project versions are created on the fly and as needed.

We recommend configuring sensitive data like access tokens and even URLs, using gitlab secrets.

```yaml
include:
    - project: synopsys/synopsys-template
      ref: main
      file: templates/unified-template.yml
  stages:
    - gitlab-template-executor
  check_template_job:
    stage: gitlab-template-executor
    tags:
      - macos
    variables:
      BLACKDUCK_URL: ${{ secrets.BLACKDUCK_URL }}
      BLACKDUCK_TOKEN: ${{ secrets.BLACKDUCK_API_TOKEN }}
      BLACKDUCK_SCAN_FULL: 'false'
    extends:
      - .run-synopsys-tools
```
**Notes :** 
- BRIDGE_VERSION, BRIDGE_URL paramters are optional and if not provided, it will download the latest bridge from artifactory
- tags will refer to runner's tagname 

# Gitlab Runner Setup

GitLab Runner can be installed and used on GNU/Linux, macOS, FreeBSD, and Windows. Refer the given documentation : 
https://docs.gitlab.com/runner/
